<%--
  Created by IntelliJ IDEA.
  User: ADAK-Shemroon
  Date: 11/7/2020
  Time: 4:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<head>
    <title>Title</title>
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <link
            rel="stylesheet"
            href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css"
            integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If"
            crossorigin="anonymous">
    <jsp:include page="employeeHeader.jsp"/>
</head>
<body dir="rtl">
<jsp:include page="body.jsp"/>
<script>
    function downloadFile(fileName) {
        window.location = '/employeeController.do?action=downloadFile&fileName=' + fileName;
    }
</script>
<c:choose>
<c:when test="${empty requestScope.receivedEmailsInfo}">
    <div style="width: 400px;border-radius: 5px; margin: 10px auto;text-align: center;">
        <div class="alert alert-info" id="info-alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong><fmt:bundle basename="resource_fa">
                <fmt:message key="no received email"/>
            </fmt:bundle>
            </strong>
        </div>
    </div>
</c:when>
<c:otherwise>
<div class="container" style="margin-top: 50px;border-radius: 6px;background-color: deepskyblue;">
    <span style="font-weight: bold;font-size: 17px;color:darkblue"><h><fmt:bundle basename="resource_fa">
        <fmt:message key="received emails list"/>
    </fmt:bundle></h></span>
    <table class="table table-bordered table-hover table-responsive-lg">
        <thead class="thead-light ">
        <tr style="border-radius: 10px;">
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="received date"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="email sender firstName"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="email sender lastName"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="received email content"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="attachment file"/>
            </fmt:bundle></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${requestScope.receivedEmailsInfo}" var="receivedEmails">
            <tr>
                <td hidden><c:out value="${receivedEmails[0]}"/></td>
                <td><c:out value="${receivedEmails[1]}"/></td>
                <td><c:out value="${receivedEmails[2]}"/></td>
                <td><c:out value="${receivedEmails[3]}"/></td>
                <td><c:out value="${receivedEmails[4]}"/></td>
                <td>
                    <c:choose>
                        <c:when test="${empty receivedEmails[5]}">
                            <span><fmt:bundle basename="resource_fa">
                                <fmt:message key="no attachment file"/>
                            </fmt:bundle></span>
                        </c:when>
                        <c:otherwise>
                            <button type="button" class="btn btn-primary btn-lm my-0 badge-pill"
                                    style="width: 110px; border-radius: 7px; align-self: center;"
                                    onclick="downloadFile('${receivedEmails[5]}')"><fmt:bundle basename="resource_fa">
                                <fmt:message key="receive file"/>
                            </fmt:bundle>
                            </button>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </c:otherwise>
    </c:choose>
</div>


<c:choose>
    <c:when test="${empty requestScope.sentEmailsInfo}">
        <div style="width: 400px;border-radius: 5px; margin: 10px auto;text-align: center;">
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <strong><fmt:bundle basename="resource_fa">
                    <fmt:message key="no sent email"/>
                </fmt:bundle>
                </strong>
            </div>
        </div>
    </c:when>
    <c:otherwise>
<div class="container" style="margin-top: 50px;border-radius: 6px;background-color: deepskyblue;">
    <span style="font-weight: bold;font-size: 17px;color:darkblue"><h><fmt:bundle basename="resource_fa">
        <fmt:message key="sent emails list"/>
    </fmt:bundle></h></span>
    <table class="table table-bordered table-hover table-responsive-lg">
        <thead class="thead-light ">
        <tr style="border-radius: 10px;">
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="sent date"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="email receiver firstName"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="email receiver lastName"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="sent email content"/>
            </fmt:bundle></th>
            <th class="text-center" scope="col"><fmt:bundle basename="resource_fa">
                <fmt:message key="attachment file"/>
            </fmt:bundle></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${requestScope.sentEmailsInfo}" var="sentEmails">
            <tr>
                <td hidden><c:out value="${sentEmails[0]}"/></td>
                <td><c:out value="${sentEmails[1]}"/></td>
                <td><c:out value="${sentEmails[2]}"/></td>
                <td><c:out value="${sentEmails[3]}"/></td>
                <td><c:out value="${sentEmails[4]}"/></td>
                <td>
                    <c:choose>
                        <c:when test="${empty sentEmails[5]}">
                            <span><fmt:bundle basename="resource_fa">
                                <fmt:message key="no attachment file"/>
                            </fmt:bundle></span>
                        </c:when>
                        <c:otherwise>
                            <button type="button" class="btn btn-primary btn-lm my-0 badge-pill"
                                    style="width: 110px; border-radius: 7px; align-self: center;"
                                    onclick="downloadFile('${sentEmails[5]}')"><fmt:bundle basename="resource_fa">
                                <fmt:message key="receive file"/>
                            </fmt:bundle>
                            </button>
                        </c:otherwise>
                    </c:choose>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </c:otherwise>
    </c:choose>
</div>


</body>
</html>

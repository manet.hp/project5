<%--
  Created by IntelliJ IDEA.
  User: ADAK-Shemroon
  Date: 10/31/2020
  Time: 5:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>

        function dateCheck() {
            var leaveFromDate = new Date($('#leaveFromDate').val());
            var leaveToDate = new Date($('#leaveToDate').val());
            if (document.forms["leaveRequestForm"]["leaveFromDate"].value == null || document.forms["leaveRequestForm"]["leaveFromDate"].value === "") {
                window.alert('<fmt:bundle basename="resource_fa"><fmt:message key="leaveFromDate not null"/></fmt:bundle>');
                return false;
            }
            if (document.forms["leaveRequestForm"]["leaveToDate"].value == null || document.forms["leaveRequestForm"]["leaveToDate"].value === "") {
                window.alert('<fmt:bundle basename="resource_fa"><fmt:message key="leaveToDate not null"/></fmt:bundle>');
                return false;
            }
            if (Date.parse(leaveFromDate) >= Date.parse(leaveToDate)) {
                window.alert('<fmt:bundle basename="resource_fa"><fmt:message key="leaveFromDate greater than leaveToDate"/></fmt:bundle>');
                return false;
            }
        }
    </script>

    <jsp:include page="employeeHeader.jsp"/>
</head>
<body dir="rtl">
<jsp:include page="body.jsp"/>
<c:if test="${requestScope['invalidLeaveRequest'] =='invalidLeaveRequest'}">
    <div style="width: 450px;border-radius: 5px; margin: 10px auto;">
        <div class="alert alert-danger" id="danger-alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong><fmt:bundle basename="resource_fa">
                <fmt:message key="overlapped leave request"/></fmt:bundle>
            </strong>
        </div>
    </div>
</c:if>
<c:if test="${requestScope['invalidLeaveRequest'] =='validLeaveRequest'}">
    <div style="width: 450px;border-radius: 5px; margin: 10px auto;">
        <div class="alert alert-success" id="success-alert">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong><fmt:bundle basename="resource_fa">
                <fmt:message key="register request successfully"/></fmt:bundle>
            </strong>
        </div>
    </div>
</c:if>


<div class="container" name="container"
     style="background-color: deepskyblue; width: 700px; margin-top: 55px;border-radius: 5px;">
    <form action="/employeeController.do" id="leaveRequestForm" method="post" onsubmit="return dateCheck()">
        <input type="hidden" name="action" value="leaveRequest">
        <div class="form-row">
            <div class="form-group col-md-6" style="margin-top: 17px;">
                <label><label style="color: #ff4626">*</label><fmt:bundle basename="resource_fa">
                    <fmt:message key="leaveFromDate"/></fmt:bundle></label>
                <input type="date" class="form-control" id="leaveFromDate" name="leaveFromDate">
            </div>
            <div class="form-group col-md-6" style="margin-top: 17px;">
                <label><label style="color: #ff4626">*</label><fmt:bundle basename="resource_fa">
                    <fmt:message key="leaveToDate"/></fmt:bundle></label>
                <input type="date" class="form-control" id="leaveToDate" name="leaveToDate">
            </div>
        </div>
        <div class="form-row" dir="rtl">
            <div class="col-md-3" style="margin-top: 17px;margin-bottom: 10px;">
                <button type="submit" class="btn btn-primary"><fmt:bundle basename="resource_fa">
                    <fmt:message key="saveLeave"/>
                </fmt:bundle></button>
            </div>
        </div>
    </form>
</div>
</body>
</html>

package controller;

import common.UsernameValidation;
import common.service.*;
import model.entity.CategoryElement;
import model.entity.Employee;
import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@WebServlet("/managerController.do")
public class ManagerController extends HttpServlet {
    boolean showDuplicateUsernameAlert;
    static Logger logger = null;

    @Override
    public void init(ServletConfig config) throws ServletException {
        logger = Logger.getRootLogger();
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = null;
        action = req.getParameter("action");
        switch (action) {
            case "save":
                save(req, resp);
                break;
            case "insertEmployee":
                insertEmployee(req, resp, showDuplicateUsernameAlert);
                break;
            case "findAll":
                findAll(req, resp);
                break;
            case "update":
                update(req, resp);
                break;
            case "inactive":
                inactive(req, resp);
                break;
            case "search":
                search(req, resp);
                break;
            case "editEmployee":
                editEmployee(req, resp);
                break;
            case "logout":
                logout(req, resp);
                break;
            case "beneathEmployees":
                beneathEmployees(req, resp);
                break;
            case "acceptLeave":
                acceptLeave(req, resp);
                break;
            case "rejectLeave":
                rejectLeave(req, resp);
                break;
        }
    }

    public void rejectLeave(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LocalDate lastModifyDataTime = LocalDate.now();
        int leaveId = Integer.parseInt(req.getParameter("leaveId"));
        LeaveEmployeeService.getInstance().changeLeaveStatusToRejected(leaveId, lastModifyDataTime);
        logger.info("leave request rejected");
        beneathEmployees(req, resp);
    }

    public void acceptLeave(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        LocalDate lastModifyDataTime = LocalDate.now();
        int leaveId = Integer.parseInt(req.getParameter("leaveId"));
        LeaveEmployeeService.getInstance().changeLeaveStatusToAccepted(leaveId, lastModifyDataTime);
        logger.info("leave request accepted");
        beneathEmployees(req, resp);
    }

    public void beneathEmployees(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Employee manager = ManagerService.getInstance().
                findManagetByUsername((String) req.getSession().getAttribute("username"));
        List<Employee> beneathEmployee = ManagerService.getInstance().findAllBeneathEmployee((manager));
        req.setAttribute("beneathEmployee", beneathEmployee);
        req.getRequestDispatcher("leaveEmployeesManagement.jsp").forward(req, resp);
    }


    public void logout(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().removeAttribute("username");
        req.getSession().invalidate();
        logger.info("user logout");
        resp.sendRedirect("login.jsp");
    }


    public void editEmployee(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<String> managerInformation = getAllManagers();
        req.setAttribute("managerList", managerInformation);


        int employeeId = Integer.parseInt(req.getParameter("employeeId"));
        Employee employee = ManagerService.getInstance().findEmployeeById(employeeId);
        Employee manager = employee.getManager();
        if (manager != null) {
            req.setAttribute("lastManager", manager.getFirstName() + "  " + manager.getLastName());
        }
        req.setAttribute("Employee", employee);
        req.getRequestDispatcher("editEmployee.jsp").forward(req, resp);

    }

    public void update(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        CategoryElement status = CategoryElementService.getInstance().findByCodeCategory(req.getParameter("employeeStatus"));

        String[] managerData = req.getParameter("selectedManager").split("  ");
        Employee selectedManager = ManagerService.getInstance().getSelectedEmployeeManager(managerData[0], managerData[1]);


        LocalDate lastModifyDataTime = LocalDate.now();


        Employee employee = EmployeeService.getInstance().findEmployeeById(Integer.parseInt(req.getParameter("id")));
        employee.setFirstName(req.getParameter("firstName"));
        employee.setLastName(req.getParameter("lastName"));
        employee.setEmail(req.getParameter("email"));
        employee.setLastModifyDataTime(lastModifyDataTime);
        employee.setEmployeeStatus(status);
        employee.setManager(selectedManager);
        if (!req.getParameter("fatherName").equals("")) {
            employee.setFatherName(req.getParameter("fatherName"));
        }
        ManagerService.getInstance().updateEmployee(employee);
        findAll(req, resp);
    }


    public void search(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String dateInString = req.getParameter("creationDataTime");
        LocalDate localDate = null;
        if (dateInString != null && !dateInString.equals("")) {

            localDate = LocalDate.parse(dateInString, formatter);
        }

        Employee employee = new Employee(req.getParameter("firstName"), req.getParameter("lastName")
                , req.getParameter("username"), localDate);
        List<Employee> employeeList = ManagerService.getInstance().searchEmployee(employee);
        req.setAttribute("employeeList", employeeList);
        req.setAttribute("firstName", req.getParameter("firstName"));
        req.setAttribute("lastName", req.getParameter("lastName"));
        req.setAttribute("username", req.getParameter("username"));
        req.setAttribute("creationDataTime", req.getParameter("creationDataTime"));
        req.getRequestDispatcher("employeeManagement.jsp").forward(req, resp);
    }


    public void inactive(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int employeeId = Integer.parseInt(req.getParameter("employeeId"));
        ManagerService.getInstance().inactiveEmployee(employeeId);
        findAll(req, resp);
    }


    public void findAll(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("employeeList", ManagerService.getInstance().findAllEmployee());
        req.getRequestDispatcher("employeeManagement.jsp").forward(req, resp);
    }


    public void insertEmployee(HttpServletRequest req, HttpServletResponse resp, boolean showDuplicateUsernameAlert) throws IOException, ServletException {

        if (req.getSession().getAttribute("invalidUsername") != null && !showDuplicateUsernameAlert) {
            req.getSession().removeAttribute("invalidUsername");
        }
        this.showDuplicateUsernameAlert = false;
        List<String> managerInformation = getAllManagers();
        req.setAttribute("managerList", managerInformation);
        req.getRequestDispatcher("insertEmployee.jsp").forward(req, resp);
    }


    public void save(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String employeeUsername = req.getParameter("username");
        UsernameValidation usernameValidation = new UsernameValidation();
        boolean invalidUsername = usernameValidation.duplicateUsername(employeeUsername);

        if (invalidUsername) {
            req.getSession().setAttribute("invalidUsername", invalidUsername);
            showDuplicateUsernameAlert = true;
            insertEmployee(req, resp, showDuplicateUsernameAlert);
            return;
        }

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
        String dateInString = req.getParameter("dateOfBirth");
        Date dateOfBirth = null;
        if (dateInString != null && !dateInString.equals("")) {
            try {
                dateOfBirth = formatter.parse(dateInString);
            } catch (ParseException e) {
                logger.error("String can not parse to Data" + e.getMessage());
            }
        }

        LocalDate creationDataTime = LocalDate.now();

        String[] managerData = req.getParameter("selectedManager").split("  ");
        Employee selectedManager = ManagerService.getInstance().getSelectedEmployeeManager(managerData[0], managerData[1]);

        CategoryElement post = CategoryElementService.getInstance().findByCodeCategory(req.getParameter("post"));
        CategoryElement gender = CategoryElementService.getInstance().findByCodeCategory(req.getParameter("gender"));
        CategoryElement status = CategoryElementService.getInstance().findByCodeCategory(req.getParameter("employeeStatus"));

        Employee employee = new Employee(req.getParameter("firstName"), req.getParameter("lastName")
                , req.getParameter("fatherName"), req.getParameter("email"), dateOfBirth, gender, post, status, req.getParameter("username"), req.getParameter("password"), selectedManager, creationDataTime);
        try {
            ManagerService.getInstance().saveEmployee(employee);
            findAll(req, resp);
        } catch (Exception e) {
            logger.error("employee didnt save " + e.getMessage());
        }
    }

    public List<String> getAllManagers() {
        List<String> managerInformation = new ArrayList<>();
        List<Employee> employeeManagerList = ManagerService.getInstance().getAllManagerEmployee();
        for (Employee managerEmployee : employeeManagerList) {
            String info = managerEmployee.getFirstName() + "  " + managerEmployee.getLastName();
            managerInformation.add(info);
        }
        return managerInformation;
    }
}

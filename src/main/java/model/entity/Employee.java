package model.entity;

import org.hibernate.annotations.SelectBeforeUpdate;

import javax.persistence.Entity;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "Employee")
@Table(name = "t_Employee")
@SelectBeforeUpdate
public class Employee extends model.entity.Entity {

    @Column(name = "c_firstName", columnDefinition = "VARCHAR(255)")
    private String firstName;
    @Column(name = "c_lastName", columnDefinition = "VARCHAR(255)")
    private String lastName;
    @Column(name = "c_fatherName", columnDefinition = "VARCHAR(255)")
    private String fatherName;
    @Column(name = "c_email", columnDefinition = "VARCHAR(255)")
    private String email;
    @Temporal(TemporalType.DATE)
    @Column(name = "c_dateOfBirth")
    private Date dateOfBirth;
    @Column(name = "c_username", columnDefinition = "VARCHAR(255)")
    private String username;
    @Column(name = "c_password", columnDefinition = "VARCHAR(255)")
    private String password;
    @ManyToOne()
    @JoinColumn(name = "c_manager")
    private Employee manager;
    @OneToMany(mappedBy = "manager")
    private Set<Employee> employees = new HashSet<Employee>();

    @ManyToOne()
    @JoinColumn(name = "c_gender")
    private CategoryElement gender;

    @ManyToOne()
    @JoinColumn(name = "c_post")
    private CategoryElement post;

    @ManyToOne()
    @JoinColumn(name = "c_employee_status")
    private CategoryElement employeeStatus;

    @OneToMany()
    @JoinColumn(name = "c_emailSenderId")
    private Set<Email> sentEmails = new HashSet<Email>();

    @OneToMany()
    @JoinColumn(name = "c_employeeId")
    private Set<LeaveEmployee> leaveList = new HashSet<LeaveEmployee>();


    public Employee(int id, String firstName, String lastName, String fatherName, String email, LocalDate lastModifyDataTime) {
        this.id =id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.email = email;
        this.lastModifyDataTime = lastModifyDataTime;

    }

    public Employee(String firstName, String lastName, String fatherName, String email, Date dateOfBirth, CategoryElement gender, CategoryElement post, CategoryElement employeeStatus, String username, String password, Employee manager, LocalDate creationDataTime) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.fatherName = fatherName;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.gender = gender;
        this.post = post;
        this.employeeStatus = employeeStatus;
        this.username = username;
        this.password = password;
        this.manager = manager;
        this.creationDataTime = creationDataTime;
    }


    public Employee(String firstName, String lastName, String username, LocalDate creationDataTime) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.creationDataTime = creationDataTime;
    }

    public Employee(int id,String firstName, String lastName,String email,String fatherName,LocalDate lastModifyDataTime,CategoryElement employeeStatus,Employee manager){
        this.id=id;
        this.firstName=firstName;
        this.lastName=lastName;
        this.email=email;
        this.fatherName=fatherName;
        this.lastModifyDataTime = lastModifyDataTime;
        this.employeeStatus = employeeStatus;
        this.manager = manager;
    }


    public Employee() {
    }

    public CategoryElement getEmployeeStatus() {
        return employeeStatus;
    }

    public void setEmployeeStatus(CategoryElement employeeStatus) {
        this.employeeStatus = employeeStatus;
    }

    public Set<LeaveEmployee> getLeaveList() {
        return leaveList;
    }

    public void setLeaveList(Set<LeaveEmployee> leaveList) {
        this.leaveList = leaveList;
    }

    public Set<Email> getSentEmails() {
        return sentEmails;
    }

    public void setSentEmails(Set<Email> sentEmails) {
        this.sentEmails = sentEmails;
    }

    public CategoryElement getGender() {
        return gender;
    }

    public void setGender(CategoryElement gender) {
        this.gender = gender;
    }

    public CategoryElement getPost() {
        return post;
    }

    public void setPost(CategoryElement post) {
        this.post = post;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }
}

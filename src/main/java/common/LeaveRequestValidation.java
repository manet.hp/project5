package common;

import common.service.EmployeeService;
import model.entity.Employee;
import model.entity.LeaveEmployee;
import org.apache.log4j.Logger;

import java.time.LocalDate;
import java.util.List;

public class LeaveRequestValidation {
    static Logger logger = Logger.getRootLogger();

    private LeaveRequestValidation() {
    }

    private static LeaveRequestValidation leaveRequestValidation = new LeaveRequestValidation();

    public static LeaveRequestValidation getInstance() {
        return leaveRequestValidation;
    }

    public boolean validLeave(LocalDate leaveFrom, LocalDate leaveTo, Employee employee) {
        boolean isValid = true;
        List<LeaveEmployee> leaveEmployeeSet = EmployeeService.getInstance().findLeaveEmployee(employee);
        if (leaveEmployeeSet != null) {
            for (LeaveEmployee leaveEmployee : leaveEmployeeSet) {
                LocalDate startLeave = leaveEmployee.getLeaveFromDate();
                LocalDate endLeave = leaveEmployee.getLeaveToDate();
                if (!leaveFrom.isAfter(endLeave) && !startLeave.isAfter(leaveTo)) {
                    logger.info("overlap occoured !!");
                    isValid = false;
                }
            }
        }
        return isValid;
    }
}

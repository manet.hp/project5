package common.service;

import common.repository.EmailDao;
import model.entity.Email;
import model.entity.Employee;

import java.util.List;

public class EmailService {
    private EmailService() {
    }

    private static EmailService emailService = new EmailService();

    public static EmailService getInstance() {
        return emailService;
    }

    public void saveEmail(Email email){
        EmailDao emailDao = new EmailDao();
        emailDao.save(email);
    }
    public List<Email> receivedEmails(Employee employee){
        EmailDao emailDao = new EmailDao();
        return emailDao.receivedEmails(employee);
    }
    public List<Object[]> receivedEmailsInfo(Employee employee){
        EmailDao emailDao = new EmailDao();
        return emailDao.receivedEmailsInfo(employee);
    }
    public List<Object[]> sentEmailsInfo(Employee employee){
        EmailDao emailDao = new EmailDao();
        return emailDao.sentEmailsInfo(employee);
    }
}

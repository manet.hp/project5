package common.service;

import common.repository.EmployeeDao;
import model.entity.Email;
import model.entity.Employee;
import model.entity.LeaveEmployee;

import java.util.List;

public class EmployeeService {

    private EmployeeService(){}

    private static EmployeeService employeeService = new EmployeeService();

    public static EmployeeService getInstance() {
        return employeeService;
    }

    public Employee findEmployeeByUsername(String username){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.findByUsername(username);
    }
    public void updateEmployee(Employee employee){
        EmployeeDao employeeDao =new EmployeeDao();
        employeeDao.updateEmployee(employee);
    }
    public void updateEmployeeLeave(Employee employee, LeaveEmployee leaveEmployee){
        EmployeeDao employeeDao = new EmployeeDao();
        employeeDao.updateLeaveEmployee(employee,leaveEmployee);
    }
    public List<LeaveEmployee> findLeaveEmployee(Employee employee){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.findLeaveEmployee(employee);
    }
    public List<Employee> getAllEmployees(){
        EmployeeDao employeeDao =new EmployeeDao();
        return employeeDao.getAllEmployees();
    }
    public Employee findEmployeeById(int id){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.findById(id);
    }
    public void updateSentEmailEmployee(Employee employee , Email email){
        EmployeeDao employeeDao = new EmployeeDao();
        employeeDao.updateSentEmail(employee,email);
    }
    public List<Employee> receivedEmailEmployees(List<Integer> employeeIds){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.receivedEmailEmployees(employeeIds);
    }
}
